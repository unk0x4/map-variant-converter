﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MapVariantConverter.IO
{
    public class EndianWriter : IDisposable
    {
        public EndianType EndianType { get; set; }
        public Stream BaseStream { get; }

        public EndianWriter(Stream stream, EndianType endianType = EndianType.LittleEndian)
        {
            BaseStream = stream;
            EndianType = endianType;
        }

        public void WriteBytes(byte[] bytes, int offset, int count)
        {
            BaseStream.Write(bytes, offset, count);
        }

        public void WriteBytes(byte[] bytes, int offset = 0)
        {
            BaseStream.Write(bytes, offset, Buffer.ByteLength(bytes));
        }

        public void WriteUInt64(ulong value)
        {
            byte[] buffer = BitConverter.GetBytes(value);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            WriteBytes(buffer);
        }

        public void WriteInt64(long value)
        {
            WriteUInt64((ulong)value);
        }

        public void WriteUInt32(uint value)
        {
            byte[] buffer = BitConverter.GetBytes(value);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            WriteBytes(buffer);
        }

        public void WriteInt16(short value)
        {
            WriteUInt16((ushort)value);
        }


        public void WriteUInt16(ushort value)
        {
            byte[] buffer = BitConverter.GetBytes(value);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            WriteBytes(buffer);
        }

        public void WriteByte(byte value)
        {
            BaseStream.WriteByte(value);
        }

        public void WriteBool(bool value)
        {
            BaseStream.WriteByte((byte)(value ? 1 : 0));
        }

        public void WriteSByte(sbyte value)
        {
            BaseStream.WriteByte((byte)value);
        }

        public void WriteInt32(int value)
        {
            WriteUInt32((uint)value);
        }

        public void ReadInt16(short value)
        {
            WriteUInt16((ushort)value);
        }

        public void WriteSingle(float value)
        {
            byte[] buffer = BitConverter.GetBytes(value);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            WriteBytes(buffer);
        }


        public void WriteString(string value, int maxLength, CharSet charSet = CharSet.Ansi)
        {
            if (charSet == CharSet.None || charSet == CharSet.Auto)
                charSet = CharSet.Ansi;

            int length = maxLength;
            if (charSet ==  CharSet.Unicode)
                length *= 2;

            var buffer = new byte[length];
            byte[] strBytes = null;

            if (value == null)
                value = "";
            if (charSet == CharSet.Ansi)
            {
                strBytes = Encoding.UTF8.GetBytes(value);
            }
            else if (charSet == CharSet.Unicode)
            {
                if (EndianType == EndianType.BigEndian)
                {
                    strBytes = Encoding.BigEndianUnicode.GetBytes(value);
                }
                else
                {
                    strBytes = Encoding.Unicode.GetBytes(value);
                }
            }
            else
            {
                throw new NotSupportedException();
            }

            Array.Copy(strBytes, buffer, strBytes.Length);
            WriteBytes(buffer);
        }

        public void Dispose() => BaseStream.Dispose();
    }
}
