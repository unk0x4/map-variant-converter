﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.IO
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    class StructureInfoAttribute : Attribute
    {
        public int Size;
    }

    [AttributeUsage(AttributeTargets.Field)]
    class StructFieldAttribute : Attribute
    {
        public int Align = 0;
        public int Offset = -1;
    }

    class StringFieldAttribute : StructFieldAttribute
    {
        public int MaxLength;
        public CharSet CharSet = CharSet.Ansi;
    }

    class ArrayFieldAttribute : StructFieldAttribute
    {
        public int MaxCount;
    }
}
