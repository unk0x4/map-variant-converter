﻿using MapVariantConverter.Blam;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.IO
{
    public class EndianReader : IDisposable
    {
        public EndianType EndianType { get; set; }
        public Stream BaseStream { get; }

        public EndianReader(Stream stream, EndianType endianType = EndianType.LittleEndian)
        {
            BaseStream = stream;
            EndianType = endianType;
        }

        public ulong ReadUInt64()
        {
            byte[] buffer = ReadBytes(8);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            return BitConverter.ToUInt64(buffer, 0);
        }

        public long ReadInt64()
        {
            return (long)ReadUInt64();
        }

        public byte[] ReadBytes(int size)
        {
            var buffer = new byte[size];
            BaseStream.Read(buffer, 0, size);
            return buffer;
        }

        public uint ReadUInt32()
        {
            byte[] buffer = ReadBytes(4);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            return BitConverter.ToUInt32(buffer, 0);
        }

        public ushort ReadUInt16()
        {
            byte[] buffer = ReadBytes(2);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            return BitConverter.ToUInt16(buffer, 0);
        }

        public float ReadSingle()
        {
            byte[] buffer = ReadBytes(4);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            return BitConverter.ToSingle(buffer, 0);
        }

        public double ReadDouble()
        {
            byte[] buffer = ReadBytes(8);
            if (EndianType == EndianType.BigEndian)
                buffer = buffer.Reverse().ToArray();
            return BitConverter.ToDouble(buffer, 0);
        }

        public byte ReadByte()
        {
            return (byte)BaseStream.ReadByte();
        }

        public sbyte ReadSByte()
        {
            return (sbyte)BaseStream.ReadByte();
        }

        public int ReadInt32()
        {
            return (int)ReadUInt32();
        }

        public short ReadInt16()
        {
            return (short)ReadUInt16();
        }

        public RealRectangle3d ReadRectangle3d()
        {
            var rectangle = new RealRectangle3d();
            rectangle.X0 = ReadSingle();
            rectangle.X1 = ReadSingle();
            rectangle.Y0 = ReadSingle();
            rectangle.Y1 = ReadSingle();
            rectangle.Z0 = ReadSingle();
            rectangle.Z1 = ReadSingle();
            return rectangle;
        }

        public RealVector3d ReadVector3d()
        {
            var vector = new RealVector3d();
            vector.I = ReadSingle();
            vector.J = ReadSingle();
            vector.K = ReadSingle();
            return vector;
        }

        public RealPoint3d ReadPoint3d()
        {
            var point = new RealPoint3d();
            point.X = ReadSingle();
            point.Y = ReadSingle();
            point.Z = ReadSingle();
            return point;
        }

        public string ReadString(int maxLength, CharSet charSet = CharSet.Ansi)
        {
            if (charSet == CharSet.None || charSet == CharSet.Auto)
                charSet = CharSet.Ansi;

            string str = "";
            if (charSet == CharSet.Ansi)
            {
                str = Encoding.UTF8.GetString(ReadBytes(maxLength));
            }
            else if (charSet == CharSet.Unicode)
            {
                var bytes = ReadBytes(maxLength * 2);

                if (EndianType == EndianType.BigEndian)
                {
                    str = Encoding.BigEndianUnicode.GetString(bytes);
                }
                else
                {
                    str = Encoding.Unicode.GetString(bytes);
                }

            }
            return str.Substring(0, str.IndexOf('\0'));
        }

        public void Dispose() => BaseStream.Dispose();
    }
}
