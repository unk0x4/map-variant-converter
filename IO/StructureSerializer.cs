﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.IO
{
    class StructureSerializer
    {
        public T Deserialize<T>(EndianReader reader)
        {
            return (T)DeserializeStruct(reader, typeof(T));
        }

        public void Serialize<T>(EndianWriter writer, T instance)
        {
            SerializeStruct(writer, typeof(T), instance);
        }

        public object SerializeStruct(EndianWriter writer, Type type, object instance, int depth = 0)
        {
            var fields = GetStructFields(type);

            var structureInfo = type.GetCustomAttribute<StructureInfoAttribute>();

            int offset = 0;
            foreach (var field in fields)
            {
                var structField = field.GetCustomAttribute<StructFieldAttribute>();
                if (structField != null)
                {
                    if (structField.Align != 0)
                    {
                        int padding = (int)(-offset & (structField.Align - 1));
                        offset += padding;
                        if (padding > 0)
                            writer.WriteBytes(new byte[padding]);

                        if (structField.Offset != -1 && offset != structField.Offset)
                        {
                            throw new InvalidDataException();
                        }
                    }
                }

                var value = field.GetValue(instance);

                var fieldOffset = writer.BaseStream.Position;
                SerializeValue(writer, field, field.FieldType, value, depth);
                offset += (int)(writer.BaseStream.Position - fieldOffset);
            }

            if (structureInfo != null && offset != structureInfo.Size)
                throw new InvalidDataException();

            return instance;
        }

        public object DeserializeStruct(EndianReader reader, Type type, int depth = 0)
        {
            var instance = Activator.CreateInstance(type);

            var fields = GetStructFields(type);

            var structureInfo = type.GetCustomAttribute<StructureInfoAttribute>();

            int offset = 0;
            foreach (var field in fields)
            {
                var fieldName = field.Name;

                var structField = field.GetCustomAttribute<StructFieldAttribute>();
                if (structField != null)
                {
                    if (structField.Align != 0)
                    {
                        int padding = (int)(-offset & (structField.Align - 1));
                        offset += padding;
                        reader.BaseStream.Position += padding;


                    }

                    if (structField.Offset != -1 && offset != structField.Offset)
                    {
                        throw new InvalidDataException(fieldName);
                    }
                }


                var fieldOffset = reader.BaseStream.Position;


                var fieldValue = DeserializeValue(reader, field, field.FieldType, depth);
                offset += (int)(reader.BaseStream.Position - fieldOffset);

                field.SetValue(instance, fieldValue);
            }



            if (structureInfo != null && offset != structureInfo.Size)
                throw new InvalidDataException();

            return instance;
        }

        private IEnumerable<FieldInfo> GetStructFields(Type type)
        {
            var fields = Enumerable.Empty<FieldInfo>();
            if (type.BaseType != null)
                fields = GetStructFields(type.BaseType);

            return fields.Concat(type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly));
        }

        public object DeserializeValue(EndianReader reader, FieldInfo fieldInfo, Type type, int depth)
        {
            object value = null;

            if (type.IsPrimitive)
            {
                return ReadValue(reader, fieldInfo, type);
            }
            else
            {
                if (type == typeof(string) || type == typeof(DateTime) || type.IsEnum)
                {
                    value = ReadValue(reader, fieldInfo, type);
                }
                else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                {
                    var arrayField = fieldInfo.GetCustomAttribute<ArrayFieldAttribute>();
                    var elementType = type.GetGenericArguments()[0];
                    value = Activator.CreateInstance(type);
                    var list = (IList)value;
                    for (int i = 0; i < arrayField.MaxCount; i++)
                    {
                        var elementValue = DeserializeValue(reader, fieldInfo, elementType, depth);
                        list.Add(elementValue);
                    }
                }
                else
                {
                    value = DeserializeStruct(reader, type, depth + 1);
                }
            }

            return value;
        }

        public void SerializeValue(EndianWriter writer, FieldInfo fieldInfo, Type type, object value, int depth)
        {
            if (type.IsPrimitive)
            {
                WriteValue(writer, fieldInfo, type, value);
            }
            else
            {
                if (type == typeof(string) || type == typeof(DateTime) || type.IsEnum)
                {
                    WriteValue(writer, fieldInfo, type, value);
                }
                else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(List<>))
                {
                    var arrayField = fieldInfo.GetCustomAttribute<ArrayFieldAttribute>();
                    var elementType = type.GetGenericArguments()[0];
                    var list = (IList)value;
                    for (int i = 0; i < arrayField.MaxCount; i++)
                    {
                        SerializeValue(writer, fieldInfo, elementType, list[i], depth);
                    }
                }
                else
                {
                    value = SerializeStruct(writer, type, value, depth + 1);
                }
            }
        }

        private object ReadValue(EndianReader reader, FieldInfo fieldInfo, Type type)
        {
            var fieldType = fieldInfo.FieldType;

            if (type == typeof(int)) return reader.ReadInt32();
            else if (type == typeof(uint)) return reader.ReadUInt32();
            else if (type == typeof(short)) return reader.ReadInt16();
            else if (type == typeof(ushort)) return reader.ReadUInt16();
            else if (type == typeof(bool)) return reader.ReadByte() != 0;
            else if (type == typeof(byte)) return reader.ReadByte();
            else if (type == typeof(sbyte)) return reader.ReadSByte();
            else if (type == typeof(float)) return reader.ReadSingle();
            else if (type == typeof(long)) return reader.ReadInt64();
            else if (type == typeof(ulong)) return reader.ReadUInt64();
            else if (type == typeof(string))
            {
                var stringField = fieldInfo.GetCustomAttribute<StringFieldAttribute>();
                return reader.ReadString(stringField.MaxLength, stringField.CharSet);
            }
            else if (fieldType == typeof(DateTime))
            {
                var value = reader.ReadInt64();
                return DateTime.FromFileTime(value);
            }
            else if (fieldType.IsEnum)
            {
                var enumType = Enum.GetUnderlyingType(fieldType);
                return ReadValue(reader, fieldInfo, enumType);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        private void WriteValue(EndianWriter writer, FieldInfo fieldInfo, Type type, object value)
        {
            var fieldType = fieldInfo.FieldType;

            if (type == typeof(int)) writer.WriteInt32((int)value);
            else if (type == typeof(uint)) writer.WriteUInt32((uint)value);
            else if (type == typeof(short)) writer.WriteInt16((short)value);
            else if (type == typeof(ushort)) writer.WriteUInt16((ushort)value);
            else if (type == typeof(bool)) writer.WriteBool((bool)value);
            else if (type == typeof(byte)) writer.WriteByte((byte)value);
            else if (type == typeof(sbyte)) writer.WriteSByte((sbyte)value);
            else if (type == typeof(float)) writer.WriteSingle((float)value);
            else if (type == typeof(long)) writer.WriteInt64((long)value);
            else if (type == typeof(ulong)) writer.WriteUInt64((ulong)value);
            else if (type == typeof(string))
            {
                var stringField = fieldInfo.GetCustomAttribute<StringFieldAttribute>();
                writer.WriteString((string)value, stringField.MaxLength, stringField.CharSet);
            }
            else if (fieldType == typeof(DateTime)) writer.WriteInt64(((DateTime)value).ToFileTime());
            else if (fieldType.IsEnum)
            {
                var enumType = Enum.GetUnderlyingType(fieldType);
                WriteValue(writer, fieldInfo, enumType, value);
            }
            else
            {
                throw new NotSupportedException();
            }

        }
    }
}
