﻿using MapVariantConverter.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.Tags
{
    class TagCache : IEnumerable<CachedTagInstance>
    {
        private uint m_baseOffset;
        private uint m_tableOffset;
        private int m_tableCount;
        private ulong m_guid;

        private List<CachedTagInstance> m_tags;

        public TagCache(Stream stream, Dictionary<int, string> names)
        {
            m_baseOffset = (uint)stream.Position;
            Load(new EndianReader(stream), names);
        }

        public IEnumerator<CachedTagInstance> GetEnumerator()
        {
            return m_tags.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();

        }

        private void Load(EndianReader reader, Dictionary<int, string> names)
        {
            reader.ReadInt32();
            m_tableOffset = reader.ReadUInt32();
            m_tableCount = reader.ReadInt32();
            reader.ReadInt32();
            m_guid = reader.ReadUInt64();
            reader.ReadUInt64();

            var offsets = new uint[m_tableCount];
            reader.BaseStream.Position = m_baseOffset + m_tableOffset;
            for (int i = 0; i < m_tableCount; i++)
                offsets[i] = reader.ReadUInt32();

            m_tags = new List<CachedTagInstance>();
            for (int i = 0; i < m_tableCount; i++)
            {
                var offset = offsets[i] + m_baseOffset;
                if (offset == 0)
                {
                    m_tags.Add(null);
                    continue;
                }
                    

                reader.BaseStream.Position = offset;

                var instance = new CachedTagInstance();
                instance.DataOffset = offset;
                instance.Checksum = reader.ReadUInt32();
                instance.Size = reader.ReadUInt32();
                var dependencyCount = reader.ReadUInt16();
                var fixupCount = reader.ReadUInt16();
                var resourceFixupCount = reader.ReadUInt16();
                var tagReferenceFixupCount = reader.ReadUInt16();
                instance.DefinitionOffset = reader.ReadUInt32();
                instance.Group = new TagGroup();
                instance.Group.Deserialize(reader);
                instance.Index = i;

                string tagName;
                names.TryGetValue(i, out tagName);
                instance.Name = tagName;

                instance.Dependencies = new List<int>();
                for (var j = 0; j < dependencyCount; j++)
                    instance.Dependencies.Add(reader.ReadInt32());
                
                instance.Fixups = new List<uint>();
                for (var j = 0; j < fixupCount; j++)
                    instance.Fixups.Add(PointerToOffset(reader.ReadUInt32()));
                
                instance.ResourceFixups = new List<uint>();
                for (var j = 0; j < resourceFixupCount; j++)
                    instance.ResourceFixups.Add(PointerToOffset(reader.ReadUInt32()));
                
                instance.TagReferenceFixups = new List<uint>();
                for (var j = 0; j < tagReferenceFixupCount; j++)
                    instance.TagReferenceFixups.Add(PointerToOffset(reader.ReadUInt32()));

                m_tags.Add(instance);
            }
        }

        public uint PointerToOffset(uint pointer)
        {
            return pointer - 0x00400000;
        }

    }

    public struct Tag
    {
        public uint Value;

        public Tag(uint value)
        {
            Value = value;
        }

        public Tag(string str)
        {
            var bytes = Encoding.ASCII.GetBytes(str);
            Value = 0;
            foreach (var b in bytes)
            {
                Value <<= 8;
                Value |= b;
            }
        }

        public override string ToString()
        {
            return Encoding.ASCII.GetString(BitConverter.GetBytes(Value).Reverse().ToArray(), 0, 4);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public static bool operator ==(Tag lhs, string rhs)
        {
            return lhs.Equals(new Tag(rhs));
        }

        public static bool operator !=(Tag lhs, string rhs)
        {
            return !lhs.Equals(new Tag(rhs));
        }
    }

    class TagGroup
    {
        public Tag Tag;
        public Tag ParentTag;
        public Tag GrandparentTag;
        public uint Name;

        public void Deserialize(EndianReader reader)
        {
            Tag = reader.ReadTag();
            ParentTag = reader.ReadTag();
            GrandparentTag = reader.ReadTag();
            Name = reader.ReadUInt32();
        } 
    }

    class CachedTagInstance
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public uint Checksum { get; set; }
        public uint Size { get; set; }
        public List<int> Dependencies { get; set; }
        public List<uint> Fixups { get; set; }
        public List<uint> ResourceFixups { get; set; }
        public List<uint> TagReferenceFixups { get; set; }
        public uint DefinitionOffset { get; set; }
        public uint DataOffset { get; set; }
        public TagGroup Group { get; set; }
    }

    static class EndianReaderExtensions
    {
        public static Tag ReadTag(this EndianReader reader)
        {
            return new Tag() { Value = reader.ReadUInt32() };
        }
    }
}
