﻿using MapVariantConverter.IO;
using MapVariantConverter.Tags;
using System;
using System.IO;

namespace MapVariantConverter.Blf
{
    class BlfReader : IDisposable
    {
        private EndianReader m_reader;

        public BlfReader(Stream stream)
        {
            m_reader = new EndianReader(stream);
            Begin();
        }

        private void Begin()
        {
            m_reader.BaseStream.Position = 0xC;
            var bom = m_reader.ReadInt16();
            if (bom != -2)
            {
                m_reader.EndianType = EndianType.BigEndian;
            }

            m_reader.BaseStream.Position = 0;
        }

        public BlfChunkV2 Readchunk()
        {
            var tag = m_reader.ReadUInt32();
            var size = m_reader.ReadInt32();
            var versionMajor = m_reader.ReadInt16();
            var versionMinor = m_reader.ReadInt16();
            var data = m_reader.ReadBytes(size - 0xC);
            return new BlfChunkV2(new Tag(tag), versionMajor, versionMinor, data);
        }

        public void Dispose()
        {
            m_reader.Dispose();
        }
    }
}
