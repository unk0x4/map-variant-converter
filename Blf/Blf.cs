﻿using MapVariantConverter.Blam;
using MapVariantConverter.IO;
using MapVariantConverter.Tags;
using System.Collections.Generic;

namespace MapVariantConverter.Blf
{
    class BlfChunkV2
    {
        public Tag Signature;
        public short VersionMajor;
        public short VersionMinor;
        public byte[] Data;

        public string AsciiTag => Signature.ToString();

        public BlfChunkV2(Tag signature, short versionMajor, short versionMinor, byte[] data)
        {
            this.Signature = signature;
            this.VersionMajor = versionMajor;
            this.VersionMinor = versionMinor;
            this.Data = data;
        }
    }

    [StructureInfo(Size = 0xE094)]
    public class BlfMapVariantChunkData
    {
        public int Version;
        public SMapVariant MapVariant;
    }

    [StructureInfo(Size = 0x24)]
    public class BlfStartOfFileChunkData
    {
        public short ByteOrderMarker;
        public short Unknown;
        [StringField(MaxLength = 32)]
        public string InternalName;
    }

    [StructureInfo(Size = 0x5)]
    public class BlfEndOfFileChunkData
    {
        public uint TotalFileSize;
        public byte AuthenticationType;
    }

    [StructureInfo(Size = 0x9)]
    public class BlfEndOfFileChunkCRCData : BlfEndOfFileChunkData
    {
        public uint Checksum;
    }

    [StructureInfo(Size = 0x105)]
    public class BlfEndOfFileChunkSHA1Data : BlfEndOfFileChunkData
    {
        [ArrayField(MaxCount = 256)]
        public uint[] Hash;
    }

    [StructureInfo(Size = 0x105)]
    public class BlfEndOfFileChunkRSAData : BlfEndOfFileChunkData
    {
        [ArrayField(MaxCount = 256)]
        public uint[] Hash;
    }

    [StructureInfo(Size = 0x10000)]
    class BlfTagNameChunkData
    {
        public struct TagNameEntry
        {
            [StringField(MaxLength = 256)]
            public string Name;


            public TagNameEntry(string name = "")
            {
                Name = name;
            }
        }

        [ArrayField(MaxCount = 256)]
        public List<TagNameEntry> Names;
    }
}
