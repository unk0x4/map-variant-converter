﻿using MapVariantConverter.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.Blf
{
    class BlfWriter : IDisposable
    {
        private EndianWriter m_writer;

        public BlfWriter(Stream stream)
        {
            m_writer = new EndianWriter(stream);
        }

        public void WriteChunk(BlfChunkV2 chunk)
        {
            m_writer.WriteUInt32(chunk.Signature.Value);
            m_writer.WriteInt32(chunk.Data.Length + 0xC);
            m_writer.WriteInt16(chunk.VersionMajor);
            m_writer.WriteInt16(chunk.VersionMinor);
            m_writer.WriteBytes(chunk.Data);
        }

        public void Dispose()
        {
            m_writer.Dispose();
        }
    }
}
