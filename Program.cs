﻿using MapVariantConverter.Blf;
using MapVariantConverter.IO;
using MapVariantConverter.Tags;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static MapVariantConverter.Blf.BlfTagNameChunkData;

namespace MapVariantConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.Out.WriteLine("Expected path to blf to be converted.");
                return;
            }

            using (var blfReader = new BlfReader(File.OpenRead(args[0])))
            using (var blfWriter = new BlfWriter(File.OpenWrite(args[1])))
            {
                BlfChunkV2 chunk = null;
                do
                {
                    var chunksToWrite = new List<BlfChunkV2>();

                    chunk = blfReader.Readchunk();
                    if (chunk.Signature == "_blf")
                    {
                        using (var ms = new EndianWriter(new MemoryStream(chunk.Data)))
                            ms.WriteInt16(-2); // fix BOM for little endian

                        blfWriter.WriteChunk(chunk);
                    }
                    else if (chunk.Signature == "mapv")
                    {
                        BlfChunkV2 tagNameChunk;
                        ConvertMapVariant(chunk, out tagNameChunk);
                        blfWriter.WriteChunk(chunk);
                        blfWriter.WriteChunk(tagNameChunk);
                        return;
                    }
                    else
                    {
                        blfWriter.WriteChunk(chunk);
                    }
                }
                while (chunk.Signature != "_eof");
            }
        }

        static void ConvertMapVariant(BlfChunkV2 chunk, out BlfChunkV2 tagNameChunk)
        {
            var tagNameChunkData = new BlfTagNameChunkData();
            tagNameChunkData.Names = new List<TagNameEntry>();

            var serializer = new StructureSerializer();

            using (var chunkStream = new MemoryStream(chunk.Data))
            {
                var chunkReader = new EndianReader(chunkStream, EndianType.LittleEndian);
                var mapv = serializer.Deserialize<BlfMapVariantChunkData>(chunkReader);
                if (mapv.MapVariant.Version != 12)
                    throw new InvalidDataException($"Unrecognised map variant version! {mapv.Version}");

                var mapVariant = mapv.MapVariant;
                var oldPalette = LoadPalette(@"palette.csv");
                foreach (var item in mapVariant.Palette)
                {
                    if (item.TagIndex == -1)
                    {
                        tagNameChunkData.Names.Add(new TagNameEntry());
                    }
                    else
                    {
                        if (!oldPalette.ContainsKey(item.TagIndex))
                            throw new InvalidDataException($"Unrecognised tag index! 0x{item.TagIndex:X08}");

                        var name = oldPalette[item.TagIndex];
                        tagNameChunkData.Names.Add(new TagNameEntry(name));
                    }
                }
            }

            using (var ms = new MemoryStream())
            {
                var chunkWriter = new EndianWriter(ms);
                serializer.Serialize(chunkWriter, tagNameChunkData);
                tagNameChunk = new BlfChunkV2(new Tag("tagn"), 1, 0, ms.ToArray());
            }
        }

        static Dictionary<int, string> LoadPalette(string path)
        {
            return File.ReadAllLines(path)
                .Select(line => line.Split(','))
                .Select(csv => new { TagIndex = Convert.ToInt32(csv[0], 16), Name = csv[1] })
                .ToDictionary(entry => entry.TagIndex, entry => entry.Name.Trim());
        }
    }
}
