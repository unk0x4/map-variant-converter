﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.Blam
{
    public enum GameEngineFlags : ushort
    {
        Ctf = 1,
        Slayer = 2,
        Oddball = 4,
        Koth = 8,
        Sandbox = 16,
        Vip = 32,
        Juggernaut = 64,
        Territories = 128,
        Assault = 256,
        Infection = 512
    }

    public enum GameEngineType : int
    {
        None,
        Ctf,
        Slayer,
        Oddball,
        Koth,
        Sandbox,
        Vip,
        Juggernaut,
        Territories,
        Assault,
        Infection,
    }


    public enum GameTeam : sbyte
    {
        Defender,
        Attacker,
        Team3,
        Team4,
        Team5,
        Team6,
        Team7,
        Team8,
        Neutral
    }
}
