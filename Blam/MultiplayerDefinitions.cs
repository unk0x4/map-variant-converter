﻿using MapVariantConverter.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter
{
    public enum MultiplayerObjectType : sbyte
    {
        Ordinary = 0x0,
        Weapon = 0x1,
        Grenade = 0x2,
        Projectile = 0x3,
        Powerup = 0x4,
        Equipment = 0x5,
        LightLandVehicle = 0x6,
        HeavyLandVehicle = 0x7,
        FlyingVehicle = 0x8,
        Teleporter2Way = 0x9,
        TeleporterSender = 0xA,
        TeleporterReceiver = 0xB,
        PlayerSpawnLocation = 0xC,
        PlayerRespawnZone = 0xD,
        HoldSpawnObjective = 0xE,
        CaptureSpawnObjective = 0xF,
        HoldDestinationObjective = 0x10,
        CaptureDestinationObjective = 0x11,
        HillObjective = 0x12,
        InfectionHavenObjective = 0x13,
        TerritoryObjective = 0x14,
        VIPBoundaryObjective = 0x15,
        VIPDestinationObjective = 0x16,
        JuggernautDestinationObjective = 0x17,
    };

    public enum MultiplayerObjectFlags : sbyte
    {

    }

    public enum MultiplayerObjectShapeType : sbyte
    {
        None,
        Sphere,
        Cylinder,
        Box
    }

    [StructureInfo(Size = 0x11)]
    public struct MultiplayerObjectBoundaryShape
    {
        public MultiplayerObjectShapeType Type;
        public float Width;
        public float Length;
        public float Top;
        public float Bottom;

        public void Deserialize(EndianReader reader)
        {
            Type = (MultiplayerObjectShapeType)reader.ReadByte();
            Width = reader.ReadSingle();
            Length = reader.ReadSingle();
            Top = reader.ReadSingle();
            Bottom = reader.ReadSingle();
        }
    }

}
