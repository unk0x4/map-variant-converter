﻿using MapVariantConverter.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.Blam
{
    [StructureInfo(Size = 0xC)]
    public struct RealVector3d
    {
        public float I;
        public float J;
        public float K;
    }

    [StructureInfo(Size = 0xC)]
    public struct RealPoint3d
    {
        public float X;
        public float Y;
        public float Z;
    }

    [StructureInfo(Size = 0x18)]
    public struct RealRectangle3d
    {
        public float X0;
        public float X1;
        public float Y0;
        public float Y1;
        public float Z0;
        public float Z1;
    }
}
