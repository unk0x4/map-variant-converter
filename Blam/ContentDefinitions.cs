﻿using MapVariantConverter.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.Blam
{
    [StructureInfoAttribute(Size = 0xF8)]
    public class ContentItemMetadata
    {
        public ulong Checksum;
        [StringField(CharSet = CharSet.Unicode, MaxLength = 16)]
        public string Name;
        [StringField(CharSet = CharSet.Ansi, MaxLength = 128)]
        public string Description;
        [StringField(CharSet = CharSet.Ansi, MaxLength = 16)]
        public string Author;
        public int ContentType;
        public bool UserIsOnline;
        [StructField(Align = 0x4)]
        public ulong UserId;
        public long Size;
        public DateTime Timestamp;
        public int Unknown;
        public int CampaignId;
        public int Mapid;
        public int GameEngineType;
        public int CampaignDifficulty;
        public byte CampaignInsertionPoint;
        public bool IsSurvival;
        [StructField(Align = 0x4)]
        public ulong GameId;
    }
}
