﻿using MapVariantConverter.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.Blam
{
    public class SMapVariant
    {
        public ContentItemMetadata Metadata;
        public short Version;
        public short ScenarioObjectCount;
        public short PlacedObjectCount;
        public short PaletteItemCount;
        public int MapId;
        public RealRectangle3d WorldBounds;
        public GameEngineType GameType;
        public float MaximumBudget;
        public float SpentBudget;
        public byte RuntimeShowHelpers;
        public byte DirtyFlag;
        [StructField(Align = 4)]
        public uint MapChecksum;
        [ArrayField(MaxCount = 640)]
        public List<SMapVariantPlacement> Placements;
        [ArrayField(MaxCount = 16)]
        public List<short> ScenarioDatumIndices;
        [ArrayField(MaxCount = 256)]
        public List<SMapVariantPaletteItem> Palette;
        [ArrayField(MaxCount = 80)]
        public List<int> SimulationEntities;
    }

    [Flags]
    public enum MapVariantPlacementFlags
    {
        Valid = 0x1,
        Touched = 0x2,
        Deleted = 0x20,
        Attached = 0x200
    }

    [StructureInfo(Size = 0x18)]
    public class SMapVariantPlacementProperties
    {
        public GameEngineFlags EngineFlags;
        public MultiplayerObjectFlags MultiplayerFlags;
        public GameTeam TeamAffiliation;
        public byte SharedStorage;
        public byte SpawnTime;
        public MultiplayerObjectType ObjectType;
        public MultiplayerObjectBoundaryShape Shape;
    }

    [StructureInfo(Size = 0x54)]
    public class SMapVariantPlacement
    {
        public MapVariantPlacementFlags Flags;
        [StructField(Align = 0x4)]
        public int ObjectIndex;
        public int EditorObjectIndex;
        public int PaletteIndex;
        [StructField(Offset = 0x10)]
        public RealPoint3d Position;
        [StructField(Offset = 0x1C)]
        public RealVector3d Forward;
        [StructField(Offset = 0x28)]
        public RealVector3d Up;
        [StructField(Offset = 0x34)]
        public ObjectIdentifier ParentScenarioObject;
        [StructField(Offset = 0x3C)]
        public SMapVariantPlacementProperties Properties;
    }

    [StructureInfo(Size = 0xC)]
    public class SMapVariantPaletteItem
    {
        public int TagIndex;
        public byte RuntimeMin;
        public byte RuntimeMax;
        public byte CountOnMap;
        public byte PlacedOnMapMax;
        public float Cost;
    }
}
