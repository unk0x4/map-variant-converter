﻿using MapVariantConverter.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariantConverter.Blam
{
    [StructureInfo(Size = 0x8)]
    public struct ObjectIdentifier
    {
        public uint UniqueId;
        public short BspIndex;
        public byte Type;
        public byte Source;

        public void Deserialize(EndianReader reader)
        {
            UniqueId = (uint)reader.ReadUInt16() << 16 | reader.ReadUInt16();
            BspIndex = reader.ReadInt16();
            Type = reader.ReadByte();
            Source = reader.ReadByte();
        }
    }
}
